var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var streamify = require('gulp-streamify');
var ngAnnotate = require('gulp-ng-annotate');
var ejs = require("gulp-ejs");

var destFolder = './static/',
    destFile = 'bundle.js';

gulp.src([	
    './source/js/config.js',
    './source/js/1.sunoglobal.js',
    './source/js/2.sunorequest.js',
    './source/js/3.sunoauth.js',
    './source/js/4.sunoproduct.js',
    './source/js/5.sunocustomer.js',
    './source/js/6.sunobasicsaleorder.js',
    './source/js/8.sunosaleordercafe.js',
	//'./source/js/queue.js',
    './source/lib/ionic/js/ionic.bundle.min.js',
    './source/lib/ionic/js/ionic-angular.min.js',
    './source/lib/ion-datetime-picker/release/ion-datetime-picker.min.js',
    './source/lib/socket.io-client/socket.io.js',
    './source/lib/angular-socket-io/socket.js',
    './source/cordova.js',
    './source/js/hotkeys.min.js',
    './source/js/jquery-2.2.3.min.js',
    './source/js/pouchdb-6.2.0.js',
    './source/js/pouchdb.find.js',
    './source/js/pouchdb.cordova-sqlite.js',
	'./source/js/pouchdb.fruitdown.min.js',
    './source/js/app.js',
    './source/js/factory.js',
    './source/js/controllers.js',
    './source/js/LoginController.js',
    './source/js/PosController.js',
    './source/js/printer.js',
    './source/js/encoder.js',
    './source/lib/AngularJS-Toaster/toaster.min.js',
    './source/js/uuid.js',
    './source/js/underscore-min.js'
])
    .pipe(ngAnnotate())
    .pipe(concat(destFile))
    .pipe(streamify(uglify()))
    .pipe(gulp.dest(destFolder));


//var randomNum = new Date().getTime();
//gulp.src('./source/index.html')
//    .pipe(ejs({
//        rev: randomNum
//    }))
//    .pipe(gulp.dest('.'));
